package wisdom.com.selenium_spider;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;

public class SeleniumTester {
	public static void main(String[] args) {
		DesiredCapabilities caps = new DesiredCapabilities();
        caps.setJavascriptEnabled(true);                //< not really needed: JS enabled by default
        //caps.setCapability("takesScreenshot", true);    //< yeah, GhostDriver haz screenshotz!
        caps.setCapability(
            PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,
            "driver/phantomjs.exe"
        );
		WebDriver driver = new PhantomJSDriver(caps);
		
		driver.get("http://item.taobao.com/item.htm?id=44366906171");
		//System.out.println(driver.getPageSource());
		WebElement we = driver.findElement(By.xpath("//*[@id='J_TabBar']/li[2]/a"));
		System.out.println(we.toString());
		System.out.println(we.getText());
		we.click();
		String context = "";
		while(true) {
			context = driver.findElement(By.xpath("//*[@id=\"reviews\"]/div[2]/div[1]/div[2]")).getText();
			if (context.contains("正在加载")) {
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			} else 
				break;
		}
		System.out.println(context);
		driver.quit();
	}
}
