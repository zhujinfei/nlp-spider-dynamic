package org.wisdomdata.writer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.wisdomdata.framework.TaskCommon;

import junit.framework.TestCase;

public class TextWriterTest extends TestCase {
	public void writeTablesTest() {
		TextWriter writer = new TextWriter();
		Map<String, List<String>> table1 = new HashMap<String, List<String>>();
		List<String> meinv = new ArrayList<String>();
		meinv.add("xiaomei");
		meinv.add("xiaohua");
		meinv.add("xiaopiaoliang");
		
		List<String> shuaige = new ArrayList<String>();
		shuaige.add("shuaige1");
		shuaige.add("shuaige2");
		shuaige.add("shuaige3");
		table1.put("meinv", meinv);
		table1.put("shuaige", shuaige);
		Map<String, Map<String, List<String>>>  tables = new HashMap<String, Map<String, List<String>>>();
		tables.put("table1", table1);
		TaskCommon task = new TaskCommon();
		task.tables = tables;
		
		try {
			writer.writeTables(tables, "tasks/taobao/result", ".csv");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
