package org.wisdomdata.selenium.processor;

import java.util.logging.Logger;

import org.wisdomdata.selenium.SeleniumAction;
import org.wisdomdata.selenium.SeleniumProcessor;

public abstract class SeleniumActionProcessor extends SeleniumProcessor{
	private final static Logger logger =
			Logger.getLogger(SeleniumActionProcessor.class.getName());
	
	private SeleniumAction action;
	
	public SeleniumAction getAction() {
		return action;
	}

	public void setAction(SeleniumAction action) {
		this.action = action;
	}
	
	private boolean actionDoFirst = false;

	public boolean isActionDoFirst() {
		return actionDoFirst;
	}

	public void setActionDoFirst(boolean actionDoFirst) {
		this.actionDoFirst = actionDoFirst;
	}
	
	private int tryActionTime = 2;
	public int getTryActionTime() {
		return tryActionTime;
	}

	public void setTryActionTime(int tryActionTime) {
		this.tryActionTime = tryActionTime;
	}

	protected boolean doAction() {
		int mark = 0;
		while(true) {
			mark++;
			try {
				this.getAction().action();
			} catch (Exception e) {
				
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				if (getTryActionTime() <= mark) {
					logger.info("can not find element, stop.");
					return false;
				}
				else 
					continue;
			}	
			return true;
		} 
	}
	
	
}
