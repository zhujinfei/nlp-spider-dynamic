package org.wisdomdata.selenium;

import java.util.List;

import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.wisdomdata.common.AbstractListExtractor;

public abstract class SeleniumElementsExtractor extends AbstractListExtractor{
	//需要运行抓取的驱动
	@Autowired
	private SearchContext searchContext;
	
	public SearchContext getSearchContext() {
		return searchContext;
	}
	public void setSearchContext(SearchContext searchContext) {
		this.searchContext = searchContext;
	}
	
	/**
	 * 抽取WebElement，准备循环时候使用
	 * */
	private List<WebElement> extractResults;
	public List<WebElement> getExtractResults() {
		return extractResults;
	}
	public void setExtractResults(List<WebElement> extractResults) {
		this.extractResults = extractResults;
	}
	
}
