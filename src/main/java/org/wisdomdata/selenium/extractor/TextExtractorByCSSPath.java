package org.wisdomdata.selenium.extractor;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.wisdomdata.selenium.SeleniumStringExtractor;


public class TextExtractorByCSSPath extends SeleniumStringExtractor{
	private String csspath;

	public String getCsspath() {
		return csspath;
	}


	public void setCsspath(String csspath) {
		this.csspath = csspath;
	}

	private List<String> extractResults;
	public List<String> getExtractResults() {
		return extractResults;
	}


	public void setExtractResults(List<String> extractResults) {
		this.extractResults = extractResults;
	}

	public void innerExtract() throws NoSuchElementException {
		
		String result = "";
		WebElement element = this.getSearchContext().findElement(By.cssSelector(this.getCsspath()));
		
		result = element.getText();
		
		this.setExtractResult(result);
	}

}
