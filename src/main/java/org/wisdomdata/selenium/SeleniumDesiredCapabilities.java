package org.wisdomdata.selenium;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.openqa.selenium.remote.DesiredCapabilities;

public class SeleniumDesiredCapabilities extends DesiredCapabilities {
	private static final long serialVersionUID = -6589064511586874938L;
	private boolean jsEnabled = true;
	
	
	public boolean isJsEnabled() {
		return jsEnabled;
	}
	public void setJsEnabled(boolean jsEnabled) {
		this.setJavascriptEnabled(jsEnabled);
		this.jsEnabled = jsEnabled;
	}


	private Map<String, String> stringArguments;
	public Map<String, String> getStringArguments() {
		return stringArguments;
	}
	public void setStringArguments(Map<String, String> stringArguments) {
		setArguments(stringArguments);
		this.stringArguments = stringArguments;
	}
	
	
	private void setArguments(Map<String, String> stringArguments) {
		Set<Entry<String, String>> set = stringArguments.entrySet();
		Iterator<Entry<String, String>> it = set.iterator();
		while (it.hasNext()) {
			Entry<String, String> en = it.next();
			this.setCapability(en.getKey(), en.getValue());
		}
	}
}
