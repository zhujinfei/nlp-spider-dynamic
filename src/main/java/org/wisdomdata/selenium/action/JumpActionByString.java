package org.wisdomdata.selenium.action;

import org.openqa.selenium.WebDriver;
import org.wisdomdata.selenium.SeleniumAction;

public class JumpActionByString extends SeleniumAction {
	private String uri;
	

	public String getUri() {
		return uri;
	}


	public void setUri(String uri) {
		this.uri = uri;
	}


	public void action() {
		((WebDriver) this.getSearchContext()).navigate().to(this.getUri());
	}

}
