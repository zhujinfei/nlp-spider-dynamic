package org.wisdomdata.selenium;

import org.openqa.selenium.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.wisdomdata.common.AbstractCrawler;

public abstract class SeleniumSpider extends AbstractCrawler {
	//需要运行抓取的驱动
	@Autowired
	private SearchContext searchContext;
	
	public SearchContext getSearchContext() {
		return searchContext;
	}
	
	public void setSearchContext(SearchContext searchContext) {
		this.searchContext = searchContext;
	}
	
	private String uri;
	public String getUri() {
		return uri;
	}
	public void setUri(String uri) {
		this.uri = uri;
	}
}
