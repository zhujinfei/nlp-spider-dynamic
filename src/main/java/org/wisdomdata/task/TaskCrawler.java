package org.wisdomdata.task;

import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.wisdomdata.framework.Processor;

public class TaskCrawler implements Runnable{
    private String xmlTaskFile;
	public String getXmlTaskFile() {
		return xmlTaskFile;
	}
	public void setXmlTaskFile(String xmlTaskFile) {
		this.xmlTaskFile = xmlTaskFile;
	}

	@SuppressWarnings("resource")
	public void run() {
		System.out.println("taskRun.....");
		FileSystemXmlApplicationContext apx = new FileSystemXmlApplicationContext(this.getXmlTaskFile()); 
		BeanFactory factory = (BeanFactory)apx;
		Processor processor = (Processor) factory.getBean("rootProcessor");
		processor.process();
		WebDriver webDriver = (WebDriver) factory.getBean("searchContext");
		webDriver.quit();
		System.out.println("taskEnd.....");
	}
}
