package org.wisdomdata.main;

import java.util.ArrayList;
import java.util.List;

//import org.wisdomdata.task.RScheduledExecutor;
import org.wisdomdata.task.TaskCrawler;

public class MainCrawler {
	private static List<String> tasksFiles;
	public static void main(String[] args) {
		initTasksFiles();
		for (String taskFile : tasksFiles) {
			TaskCrawler crawler = new TaskCrawler();
			crawler.setXmlTaskFile(taskFile);
//			RScheduledExecutor e = new RScheduledExecutor();
//			e.setDelayTime(5);
//			e.setIntervalTime(10);
//			e.setTimeUnit('s');
//			e.setTaskCrawler(crawler);
//			e.schedule();
			crawler.run();
		}
	}
	private static void initTasksFiles() {
		tasksFiles = new ArrayList<String>();
		//将所有任务依次添加到这里测试
		tasksFiles.add("tasks/test_ghostdriver/jd_1product.xml");
	}
}
