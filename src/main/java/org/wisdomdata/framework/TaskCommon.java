package org.wisdomdata.framework;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.logging.Logger;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

public class TaskCommon {
	private final static Logger logger =
			Logger.getLogger(TaskCommon.class.getName());
	public Map<String, Map<String, List<String>>> tables
					= new LinkedHashMap<String, Map<String, List<String>>> ();
	public Stack<String> stack = new Stack<String>();
	
	//整个任务的总目录
	private String taskRootFile;
	public String getTaskRootFile() {
		return taskRootFile;
	}
	public void setTaskRootFile(String taskRootFile) {
		this.taskRootFile = taskRootFile;
	}
	private String suffix;
	

	public String getSuffix() {
		return suffix;
	}
	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}
	@Autowired
	private DataWriter writer;
	public DataWriter getWriter() {
		return writer;
	}
	public void setWriter(DataWriter writer) {
		this.writer = writer;
	}
	
	//数据源信息存放路径
	private String tableMetaData;
	public String getTableMetaData() {
		return tableMetaData;
	}
	public void setTableMetaData(String tableMetaData) {
		this.tableMetaData = tableMetaData;
	}
	
	public void writeTables() {
		try {
			this.getWriter().writeTables(this.tables, this.getTaskRootFile(), this.getSuffix());
			this.tables.clear();
		} catch (IOException e) {
			logger.warning("can not write tables");
		}
	}
	public int getTableNumber(String tableName) {
		File root = new File(this.getTaskRootFile());
		File file = new File(root, this.getTableMetaData());
		FileReader reader = null;
		try {
			reader = new FileReader(file);
			List<String> contents = IOUtils.readLines(reader);
			for (String content : contents) {
				String[] line = StringUtils.split(content, ',');
				if (line[0].equalsIgnoreCase((tableName)))
					return Integer.parseInt(line[1]);
			}
 		} catch (FileNotFoundException e) {
			logger.severe("TaskCommon: store meta data file not exist!" + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			logger.severe("TaskCommon: store meta data file can not read" + e.getMessage());
		} finally {
			IOUtils.closeQuietly(reader);
		}
		
		return 0;
	}
	public void setTableNumber(String tableName, int currentTotalNumber) {
		//将新的currentTotalNumber存入持久层
		File root = new File(this.getTaskRootFile());
		File file = new File(root, this.getTableMetaData());
		FileReader reader = null;
		FileWriter writer = null;
		try {
			reader = new FileReader(file);
			List<String> contents = IOUtils.readLines(reader);
			IOUtils.closeQuietly(reader);
			int length = contents.size();
			int i = 0;
			for (i = 0; i < length; i++) {
				String[] line = StringUtils.split(contents.get(i), ',');
				if (line[0].equalsIgnoreCase((tableName))) {
					line[1] = currentTotalNumber + "";
					String content = StringUtils.join(line, ',');
					contents.remove(i);
					contents.add(i, content); 
					break;
				}
			}
			if (i >= length) {
				contents.add(tableName + "," + currentTotalNumber);
			}
			writer = new FileWriter(file);
			IOUtils.writeLines(contents, null, writer);
			
 		} catch (FileNotFoundException e) {
			logger.severe("TaskCommon: store meta data file not exist!" + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			logger.severe("TaskCommon: store meta data file can not read" + e.getMessage());
		} finally {
			IOUtils.closeQuietly(reader);
			IOUtils.closeQuietly(writer);
		} 
	}
}
