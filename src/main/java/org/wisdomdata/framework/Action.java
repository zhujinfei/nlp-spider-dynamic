package org.wisdomdata.framework;

/**
 * 网页抓取者，网页抓取根据URL抓取网页
 * @author Clebeg
 * @time 2014-12-06
 * @version v1
 * */
public interface Action {
	void action() throws Exception;
}
