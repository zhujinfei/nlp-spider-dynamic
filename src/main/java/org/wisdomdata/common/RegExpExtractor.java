package org.wisdomdata.common;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 正则表达式抽取器，通过这个抽取器，将抽取匹配正则表达式的指定部分
 * @author Clebeg
 * @time 2014-12-07
 * @version v1
 * */
public class RegExpExtractor extends SecondExtractor {
	
	private String patternString;
	public String getPatternString() {
		return patternString;
	}
	public void setPatternString(String patternString) {
		this.patternString = patternString;
	}

	private int groupId;
	
	public int getGroupId() {
		return groupId;
	}
	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}
	public void innerExtract() {
		String result = "";
	
		Pattern pattern = Pattern.compile(this.getPatternString());
		Matcher matcher = pattern.matcher(this.getSourceText());
		if (matcher.find()) {
			if (matcher.groupCount() >= groupId) 
				result = matcher.group(groupId);
		}
		this.setExtractResult(result.trim());
	}
	

}
